# pretty mathematical structures animated
__author__ = "Armand Collin"

from graphics import *
import math
import random

def main():
    win = GraphWin('Voici une spirale', 600, 600, autoflush=False)
    win.setBackground('black')

    #SPIRALE
    for i in range(500):
        angle = 2*math.pi*i/40
        x = 300+int(i/2)*math.cos(angle)
        y = 300+int(i/2)*math.sin(angle)
        pt = Point(x,y)
        if i == 0: last = pt
        line = Line(pt, last)
        last = pt

        line.draw(win)
        line.setOutline(color_rgb(100, int(i/2), 100))
        update(100)

    print('Fin de visualisation')

    win.close()

    win = GraphWin('Voici un curieux disque', 600, 600, autoflush=False)
    win.setBackground('black')

    # RANDOM DISK
    for i in range(500):
        ang = random.random()*2*math.pi
        x = 300 + 200*math.cos(ang)
        y = 300 + 200*math.sin(ang)
        pt = Point(x,y)
        if i == 0: last = pt
        line = Line(pt, last)
        line.setOutline(color_rgb(255-int(i/2), int(i/2), 100))
        line.draw(win)

        last = pt
        update(30)

    win.close()

    win = GraphWin('Voici des rosaces', 600, 600, autoflush=False)
    win.setBackground('black')

    # ROSACE
    xShift = [150, 150, 150, 300, 450, 450, 450, 300, 300]
    yShift = [150, 300, 450, 450, 450, 300, 150, 150, 300]
    k = [6/5, 3/8, 4/7, 2/7, 7/8, 4/9, 1/8, 4/5, 7/4]
    r = [102, 51, 0, 0, 0, 0, 0, 0, 0]
    g = [255, 255, 255, 204, 153, 102, 102, 153, 204]
    b = [178, 153, 128, 102, 76, 51, 0, 0, 0]
    for j in range(9):
        for i in range(500):
            ang = 2*math.pi*i/50
            x = xShift[j]+100*math.cos(k[j]*ang)*math.cos(ang)
            y = yShift[j]+100*math.cos(k[j]*ang)*math.sin(ang)
            pt = Point(x,y)
            if i == 0 : last = pt
            ligne = Line(pt, last)
            # ligne.setOutline(color_rgb(int(i/2), 100, 255-int(i/2)))
            ligne.setOutline(color_rgb(r[j], g[j], b[j]))
            ligne.draw(win)
            last = pt

            update(100)

    win.close()

    win = GraphWin('Voici une rose de Maurer', 600, 600, autoflush=False)
    win.setBackground('black')

    # MAURER ROSE
    ns = [2,6,3,6,2]
    ds = [29, 71, 47, 97, 19]
    red = [80, 100, 150, 200, 255, 180, 200, 255, 255]
    green = [255, 200, 150, 0, 0, 102, 50, 20, 0]
    blue = [178, 120, 80, 0, 0, 51, 0, 0, 0]
    last = Point(300, 300)
    for j in range(5):
        for item in win.items[:]:
            item.undraw()
        win.update()
        n,d = ns[j], ds[j]
        for i in range(361):
            r = 300*math.sin(math.radians(n*d*i))
            ang = d*i
            x = 300 + r*math.cos(math.radians(ang))
            y = 300 + r*math.sin(math.radians(ang))
            pt = Point(x,y)
            ligne = Line(last, pt)
            ligne.setOutline( color_rgb(red[j],green[j],blue[j]) )
            ligne.draw(win)
            last = pt

            update(60)
    print('Fin de visualisation')

    win.getMouse()
    win.close()

main()
